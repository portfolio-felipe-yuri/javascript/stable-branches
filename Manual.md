## **Criando Branches Automáticas**

Com o intuito de facilitar o andamento das OS’s de manutenção e implementação a equipe DevOps criou um recurso que nos permite criar uma branch a partir de um estado verde da versão desejada.
Visto que muitas vezes precisamos recriar nossas branches pelo simples fato de terem sido criadas no período que a versão base estava quebrada.

> **Posso criar minha branch da forma que já estou acostumado?**
> Afirmamos que a o modo tradicional de criação de branches, sendo tanto pela própria interface do GitLab quanto pelo Git Bash (linha de comando) será depreciado, havendo uma trava que não mais permitirá o uso desses recursos.
>
> **Meu trabalho exige sempre o estado mais atualizado da versão!**
> Entendemos também que algumas squads ou desenvolvedores em específico dão preferência a versão mais atual dos fontes. Portanto, ainda será possível criar a branch pegando o último estado disponível no GitLab, e vamos mencioná-lo logo abaixo.

---

## **Modelo Prático**

Apenas quatro passos são necessaários para realizar a configuração e execução da pipeline, e são eles:

   1. **Posicionando no repositório**

      Para começar, temos de ir a página inicial do repositório onde está a solução, se você estiver lendo este passo a passo do repositório já estará posicionado na tela inicial, se não estiver basta [clicar aqui](https://git.company.com.br/plataforma-w/branch-estavel).

      ![Cara do repositório](images/pagina-inicial.png "Página Inicial")
      
   ---
      
   2. **Acessando as pipelines**

      No lado esquerdo da tela podemos ver os menus do repositório, clique em CI/CD, mostrado abaixo:

      ![Apertando botão de CI/CD](images/acessando-pipelines.png "Menus Repositório")

   ---   
      
   3. **Iniciando uma pipeline**

      Na tela exibida clique no botão **Run Pipeline**, mostrado abaixo:

      ![Apertando botão Run Pipeline](images/iniciando-uma-pipeline.png "Pipeline Inicial")

   ---

   4. **Configurando uma nova pipeline**

      É nesta tela que podemos setar as informações para criar as nossas branches, devemos inserir as **variáveis** que correspondem com as informações corretas para nossa branch, nesses campos abaixo:

      ![Mostrando campos para preenchimento](images/configurando-nova-pipeline.png "PConfig Vazio")

      Sendo as variáveis definidas como:

      - **BRANCH** ou **Branch** ou **branch** - [**REQUERIDO**] -> nome da branch que vamos criar
      - **ESTAVEL** ou **Estavel** ou **estavel** - [**VALORES ACEITOS**: **S** ou **N**] [**PADRÃO PREDEFINIDO**: **S**] -> estado da branch de versão na qual a sua será baseada - no caso de não ser passada está variável, então o padrão predefinido é utilizado 
      - **ORIGEM** ou **Origem** ou **origem** [**OPCIONAL**] -> branch de origem desejada pelo usuário
  
      > **!ATENÇÃO**: A variável "**ORIGEM**" possui precedência sobre necessidades de branch estável! Assim, uma branch criada de origem **NÃO PASSARÁ POR VALIDAÇÃO DE BUILD E ANULARÁ O VALOR DA VARIÁVEL 'ESTAVEL'**. Por favor, esteja ciente do comportamento, **e em caso de dúvidas, consulte o seu líder!**

      Veja abaixo exemplos de criação de branch:
         
      - **BRANCH ESTÁVEL** para desenvolvimento sobre a versão **4.11-DEV**:

         ![Campos preenchidos para versão estável 4.11](images/pipeline-4.11-estavel.png "PConfig Desenvolvimento")

      - **BRANCH NÃO ESTÁVEL** para desenvolvimento sobre a versão **4.12-DEV**:

         ![Campos preenchidos para versão não estável 4.12](images/pipeline-4.12-nao-estavel.png "PConfig Desenvolvimento")

      - **BRANCH DE ORIGEM** para desenvolvimento
          > Como já explicitado, essa opção é avançada e **ANULA OPERAÇÕES DE BRANCH ESTÁVEL**, logo, essa branch será criada diretamente da **HEAD** da branch de origem aqui passada]:

         ![Campos preenchidos para branch com origem](images/pipeline-origem.png "PConfig Desenvolvimento")

   5. **Rodando a pipeline**

      Após configurar sua pipeline, clique no botão "Run pipeline", indicado abaixo:
   
      ![Mostrando botão que salvará as configurações da pipeline](images/rodando-pipeline.png "Salvando Pipeline")
   
      Então, na próxima tela, aperte o botão de **execute**, indicado abaixo, que executará a pipeline:

      ![Botões que dão a mágica no negócio](images/executa-pipeline.png "Run Buttons")

      Pronto!
   
   ---

   ## **Log de execução**

   Após iniciarmos a execução do job, se precisarmos acompanhar o status da nossa solicitação, temos esse recurso de maneira simples.
   
   Abaixo do botão que apertamos para iniciar, podemos clicar para acessar o log de execução, como abaixo:
     
   ![Mostrando região para conferir log](images/botao-log-de-execucao.png "Button Log")

   Olhando o log, conseguimos conferir o status da nossa solicitação para criar a branch.

   Veja a baixo onde as informações podem ser extraídas. para um caso de sucesso ou falha.
      
   ![Registros de onde as coisas dao certo](images/log-de-execucao.png "Log Register")
