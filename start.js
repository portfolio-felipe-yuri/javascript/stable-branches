const knex = require("knex");
const axios = require("axios");

//=================================================// 
//                   ## PRELUDE ##                 //
//================================================//
function erro(mensagem) {
  console.log(`ERRO! => ${mensagem}`)
  process.exit(1)
}

let modoEstavel = process.env.ESTAVEL || process.env.Estavel || process.env.estavel || "S"
let branchOrigem = process.env.ORIGEM || process.env.Origem || process.env.origem || undefined
let branchASerCriada = process.env.BRANCH || process.env.Branch || process.env.branch || erro('Branch não informada!')
const commitAuthor = process.env.GITLAB_USER_EMAIL

branchASerCriada = branchASerCriada.toUpperCase()
const REGEX = /((\d{6,10})-(TRUNK|MASTER|\d+\.\d+(?:\.\d+)?(?:-RC|-DEV)?))(?:b(\d+))?/
let versaoDesejada = branchASerCriada.match(REGEX)

if (!versaoDesejada || !versaoDesejada[3]) {
  erro(`Nome da sua branch, passada na variável 'branch', está fora do padrão!`)
}

if (versaoDesejada[3] == "TRUNK" || versaoDesejada[3] == "MASTER") {
  erro("Não é permitido criar branches a partir da master (trunk)!")
}
let versaoRegistroCmp = versaoDesejada[3]

let origemUsuario = Boolean(branchOrigem)

console.log(`Usuário: '${commitAuthor}'`)
console.log(`Branch desejada: '${branchASerCriada}'`)
console.log(`Criação estável?: '${modoEstavel == "S" ? "Sim" : "Não"}'`)
console.log(`Branch de origem?: '${origemUsuario ? `Sim (usando branch: ${branchOrigem})'` : `Não (usando versão: ${versaoRegistroCmp})'`}`)
if (origemUsuario && modoEstavel == "S") {
  console.log("\n==================== AVISO! ====================")
  console.log("Opção 'ESTAVEL' será sobreposta por 'ORIGEM'!")
  console.log("POR FAVOR, LEIA A DOCUMENTAÇÃO DISPONÍVEL EM:")
  console.log("https://git.company.com.br/group/branch-estavel/-/blob/master/Manual.md")
  console.log("================================================\n")
}

//=================================================// 
//                    ## LIB ##                    //
//================================================//
const db = knex({
  client: "mysql",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
  },
})

const DB_CENTRAL_DOWNLOADS = knex({
  client: "mysql",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_CENTRAL,
  },
})

async function goDeleteCommitsDbscript() {
  try {
    return DB_CENTRAL_DOWNLOADS.table("table")
      .where("branchName", branchASerCriada)
      .del()
  } catch (err) {
    erro(
      `Falha ao deletar commits da tabela referentes a branch '${branchASerCriada}' - 
      por favor, contate a equipe DevOps!\n`
    )
  }
}

async function goCreateBranch(branch, referencia) {
  try {
    let uri = `${LINK}/branches?branch=${branch}&ref=${referencia}`

    return axios
      .post(
        uri,
        null,
        {
          headers: {
            "PRIVATE-TOKEN": process.env.GITLAB_TOKEN
          }
        }
      )
  } catch (err) {
    erro(`Falha ao tentar criar a branch '${branch}' da referência '${referencia}' - 
    por favor, contate a equipe DevOps!\n`)
  }
}

async function goVerifyBranchRecreated(branch, referencia) {
  try {
    return db("")
      .select("*")
      .where({ branchname: branch })
      .then(result => {
        let recreate = 'N'
        if (!(result.length == 0)) {
          recreate = 'S'
        }
        return db("")
          .insert({
            branchname: branch,
            hash: referencia,
            dtcreate: new Date(),
            author: commitAuthor.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi).toString(),
            recreate: recreate
          })
      })
  } catch (err) {
    erro(`Não foi possível verificar a criação da branch '${branch}' - 
    por favor, contate a equipe DevOps!\n`)
  }
}

async function goCheckVersaoEstavel(versao) {
  try {
    return db("")
      .select("")
      .where({ quebroucmp: "N", versao: versao })
      .orderBy("", "desc")
      .limit(1)
  } catch (err) {
    erro(`Não foi possível buscar dados da versão estável '${versao}' - 
    por favor, contate a equipe DevOps!\n`)
  }
}

//=================================================// 
//                    ## EXEC ##                   //
//================================================//
async function doBranching() {
  let deletedEntries = await goDeleteCommitsDbscript()

  var referencia;
  if (origemUsuario) {
    referencia = branchOrigem
  } else {
    if (modoEstavel == "S") {
      let buildsistemaEntries = await goCheckVersaoEstavel(versaoRegistroCmp)
      referencia = buildsistemaEntries[0].hashcommit
    } else {
      referencia = versaoRegistroCmp
    }
  }

  let createdResponse = await goCreateBranch(branchASerCriada, referencia)
  if (createdResponse.status != 201) {
    erro(`NÃO criada branch '${branchASerCriada}' vinda de '${origemUsuario ? branchOrigem : versaoRegistroCmp}'!\n
    Verifique se a sua branch '${branchASerCriada}' já existe!\nEm caso de dúvidas, contate a equipe DevOps!`)
  }

  let verified = await goVerifyBranchRecreated(branchASerCriada, referencia)
}

doBranching()
  .then(() =>{
    console.log(`SUCESSO! => Criada branch '${branchASerCriada}' vinda de '${origemUsuario ? branchOrigem : versaoRegistroCmp}'!`)
    process.exit(0)
  })
  .catch(err => {
    erro(err)
  })
